import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { MovieSearchComponent } from './components/movie-search/movie-search.component';
import { MovieItemComponent } from './components/movie-search/movie-item/movie-item.component';
import { HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule }   from '@angular/forms';
import { BannerComponent } from './components/header/banner/banner.component';

@NgModule({
  declarations: [
    AppComponent,
    MovieSearchComponent,
    MovieItemComponent,
    BannerComponent,

   
  ],
  imports: [
    BrowserModule,   
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
