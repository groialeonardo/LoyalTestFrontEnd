import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MoviesDTO } from '../model/MoviesDTO';

const httpOptions = {
  headers : new HttpHeaders ({
    'Content-Type':'application/json',   
  })
}

@Injectable({
  providedIn: 'root'
})
export class MovieServiceService {
  apiUrl = environment.apiUrlRoot + "/Titles";


  constructor( private httpClient:HttpClient ) { }

  getAllMovieTitles(substr:String) :  Observable<MoviesDTO> {

    return this.httpClient.get<MoviesDTO>(this.apiUrl+"/?Title="+substr);    

  }

  getMovieTitlesByPage(substr:String, pageNumber:number) :  Observable<MoviesDTO> {

    return this.httpClient.get<MoviesDTO>(this.apiUrl+"/?Title="+substr+"&Page="+pageNumber);    

  }

}