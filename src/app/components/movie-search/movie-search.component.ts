import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MoviesDTO } from 'src/app/model/MoviesDTO';
import { MovieServiceService } from 'src/app/services/movie-service.service';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.css']
})
export class MovieSearchComponent implements OnInit {

  form:FormGroup;
  moviesDTO!:MoviesDTO;
  traerTodas:boolean = true;

  
  constructor(private movieService: MovieServiceService,
    private formBuilder:FormBuilder) { 
      this.moviesDTO = new MoviesDTO;
      this.form=this.formBuilder.group({
        title:['',[Validators.required]],
        page:[Number,[Validators.required,Validators.max(1),Validators.min(1)]],   //mirar esto     
      })
      
    }

  ngOnInit(): void {
  }
  
  get Title(){
    return this.form.get('title')
  }

  get Page(){
    return this.form.get('page')
  }

  onChange(){
    this.traerTodas = !this.traerTodas;
    this.refreshValidations();
    this.Page?.setValue(1);
    console.log("pagevalue "+this.Page?.value)

   }

   onTitleChange(){
    console.log("Title changed")
    this.moviesDTO.titles=[];
    this.moviesDTO.total_pages=0;
    this.moviesDTO.total=0;   
    this.refreshValidations();
    this.Page?.setValue(1);


    console.log("pagevalue "+this.Page?.value)
   }

  getMovieTitles(event:Event){
    event.preventDefault;
    if (this.traerTodas ) {
      this.movieService.getAllMovieTitles(this.form.value.title)
      .subscribe((titlescallback)=>{
        this.moviesDTO=titlescallback;
        this.refreshValidations();    
      });

    }
    else{
      this.movieService.getMovieTitlesByPage(this.form.value.title,
      this.form.value.page)
      .subscribe((titlescallback)=>{
        this.moviesDTO=titlescallback;
        this.refreshValidations();        
        });
    }
  }

  refreshValidations(){

    this.Page?.setValidators(null);
    if (this.moviesDTO.total_pages == 0){
        this.Page?.setValidators([Validators.required,
        Validators.max(1),
        Validators.min(1)]);        
    }else{
      this.Page?.setValidators([Validators.required,
        Validators.max(this.moviesDTO.total_pages),
        Validators.min(1)]);        
    }
    this.Page?.updateValueAndValidity();
    /*this.Page?.setValue(1);*/
    console.log("pagevalue "+this.Page?.value)
  }


}
